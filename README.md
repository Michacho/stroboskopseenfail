# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://Michacho@bitbucket.org/Michacho/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/Michacho/stroboskop/commits/ca0d3ff77ae30eb4bf2f63d11ac0c1af18261bf0

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/Michacho/stroboskop/commits/fd8e4917a6cebf3a8571cc8647a0b53b22dcd6fb

Naloga 6.3.2:
https://bitbucket.org/Michacho/stroboskop/commits/ec35636e73e52f03dfbd3778fb73430c9676a120

Naloga 6.3.3:
https://bitbucket.org/Michacho/stroboskop/commits/a9a1e4a533247ff8ce00963ee826a1cc1c5cbca4

Naloga 6.3.4:
https://bitbucket.org/Michacho/stroboskop/commits/9e1339f6d8e2a82270d774405451449b6d0028ec

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/Michacho/stroboskop/commits/61386eb557bc23f1c1fe47c6ab203df5bf07c5dd

Naloga 6.4.2:
https://bitbucket.org/Michacho/stroboskop/commits/358a75510ace7b162a5d318d800be18eb2d65d58

Naloga 6.4.3:
https://bitbucket.org/Michacho/stroboskop/commits/a5797e183974993533654866b456c79882ac652c

Naloga 6.4.4:
https://bitbucket.org/Michacho/stroboskop/commits/6e4d45b3a5bbf1f4085d2363cd3b32ab2ba8ff31